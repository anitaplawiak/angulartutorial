import { NgModule }       from '@angular/core';
import { BrowserModule }  from '@angular/platform-browser';
import { FormsModule }    from '@angular/forms';
import { HttpClientModule }    from '@angular/common/http';
import { HttpClientInMemoryWebApiModule } from 'angular-in-memory-web-api';
import { InMemoryDataService }  from './in-memory-data.service';
import { AppRoutingModule }     from './app-routing.module';
import { ModalModule, BsModalService } from 'ngx-bootstrap/modal'; 
import { DataTableModule } from 'angular5-data-table';
import { DatePipe } from '@angular/common';

import { AppComponent }         from './app.component';
import { DashboardComponent }   from './dashboard/dashboard.component';
import { CustomerDetailComponent }  from './customer-detail/customer-detail.component';
import { CustomersComponent }      from './customers/customers.component';
import { CustomerService }          from './customer.service';
import { CarsComponent } from './cars/cars.component';
import { CarDetailComponent } from './car-detail/car-detail.component';
import { CarService }          from './car.service';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { LoginFormComponent } from './login-form/login-form.component';
import { RouterModule, Routes } from '@angular/router';
import { RentalsComponent } from './rentals/rentals.component';
import { RentalDetailComponent } from './rental-detail/rental-detail.component';
import { RentalService }          from './rental.service';
import { NavBarComponent } from './nav-bar/nav-bar.component';
import { CarReservationComponent } from './car-reservation/car-reservation.component';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';

 const appRoutes:Routes = [
  {
    path: '',
    component: LoginFormComponent
  },
  {
    path: 'dashboard',
    component: DashboardComponent
  }
]

@NgModule({
  imports: [
    ModalModule.forRoot(),
    RouterModule.forRoot(appRoutes),
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    HttpClientModule,
    Ng2SearchPipeModule,
    DataTableModule,
    BsDatepickerModule.forRoot(),
    // The HttpClientInMemoryWebApiModule module intercepts HTTP requests
    // and returns simulated server responses.
    // Remove it when a real server is ready to receive requests.
    HttpClientInMemoryWebApiModule.forRoot(
      InMemoryDataService, { dataEncapsulation: false }
    )
  ],
  declarations: [
    AppComponent,
    DashboardComponent,
    CustomersComponent,
    CustomerDetailComponent,
    CarsComponent,
    CarDetailComponent,
    LoginFormComponent,
    RentalsComponent,
    RentalDetailComponent,
    NavBarComponent,
    CarReservationComponent,
  ],
  providers: [ 
    CustomerService, 
    CarService, 
    RentalService,
    BsModalService,
    DatePipe
  ],
  bootstrap: [ AppComponent ]
})
export class AppModule { }