import { Component, OnInit } from '@angular/core';

import { Customer } from '../customer';

@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.css']
})
export class NavBarComponent implements OnInit {

  currentUser: Customer;

  constructor() { }

  ngOnInit() {
    this.initCurrentUser();
  }

  initCurrentUser(): void {
    var user = localStorage.getItem('currentUser');
    this.currentUser = JSON.parse(user) as Customer; 
  }
  

  isAdmin(): boolean {
    return this.currentUser.role.toLowerCase() == "admin";
  }

}
