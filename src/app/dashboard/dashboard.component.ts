import { Component, OnInit } from '@angular/core';

import { Customer } from '../customer';
import { CustomerService } from '../customer.service';
import { Car } from '../car';
import { CarService } from '../car.service';


@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: [ './dashboard.component.css' ]
})
export class DashboardComponent implements OnInit {

  currentUser: Customer;
  customers: Customer[] = [];
  cars: Car[] = [];


  constructor(private customerService: CustomerService, private carService: CarService) { }

  ngOnInit() {
    this.getCustomers(),
    this.getCars();
    this.initCurrentUser();
  }

  getCustomers(): void {
    this.customerService.getCustomers()
      .subscribe(customers => this.customers = customers.slice(2, 6));
  }
  getCars(): void {
    this.carService.getCars()
      .subscribe(cars => this.cars = cars.slice(1, 5));
  }
  

  initCurrentUser(): void {
    var user = localStorage.getItem('currentUser');
    this.currentUser = JSON.parse(user) as Customer; 
  }

  isAdmin(): boolean {
    return this.currentUser.role.toLowerCase() == "admin";
  }

}

