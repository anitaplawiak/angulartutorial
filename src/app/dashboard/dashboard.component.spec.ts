import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { Component, DebugElement } from '@angular/core';
import { DashboardComponent } from './dashboard.component';
import { Router } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { HttpModule } from '@angular/http';
import { CustomerService } from '../customer.service';
import { CarService } from '../car.service';
import { RentalService } from '../rental.service';
import { RouterLinkDirectiveStub } from '../../testing/router-link-directive-stub';
import { By } from '@angular/platform-browser';
import { asyncData } from '../../testing/async-observable-helpers';
import { defer } from 'rxjs/observable/defer';
import { of } from 'rxjs/observable/of';

@Component({ selector: 'router-outlet', template: '' })
class RouterOutletStubComponent { }

@Component({ selector: 'app-nav-bar', template: 'dashboard' })
class NavBarComponentStubComponent { }

let dashboardComponent: DashboardComponent;
let fixture: ComponentFixture<DashboardComponent>;
let routerLinks: RouterLinkDirectiveStub[];
let linkDes: DebugElement[];


describe('DashboardComponent', () => {
  beforeEach(async(() => {
    const carServiceSpy = jasmine.createSpyObj('CarService', ['getCars']);
    const customerServiceSpy = jasmine.createSpyObj('CustomerService', ['getCustomers', 'initCurrentUser']);
    const rentalServiceSpy = jasmine.createSpyObj('RentalService', ['getRentals']);

    const cars = [
      { id: 1, mark: 'Opel', model: 'Astra', colour: 'black', description:'Good car for crazy people', 
        productionDate: '2000-01-01', retirementDate: '2020-12-31', mileage: 250000, servicePeriodStartDate: '2018-02-10', servicePeriodFinishDate: '2018-02-20' },
      { id: 2, mark: 'Opel', model: 'Vectra', colour: 'green', description:'Fast like a rocket', 
      productionDate: '1995-03-01', retirementDate: '2030-09-25', mileage: 100000, servicePeriodStartDate: '2018-04-15', servicePeriodFinishDate: '2018-04-18'  },
    ];
    const customers = [
      { id: 1, firstname: 'Anita', lastname: 'Pławiak', email: 'a.pla@example.com', password: 'apla', role: 'admin' },
      { id: 2, firstname: 'Wojciech', lastname: 'Czerwiński', email: 'w.cze@example.com', password: 'wcze', role: 'user' },
    ];
    const rentals = [
      { id: 1, startDate: '2018-03-15', finishDate: '2018-03-22', carId: 1, customerId: 1, accepted: true },
      { id: 2, startDate: '2018-01-05', finishDate: '2018-01-09', carId: 2, customerId: 3, accepted: true },
    ];


      TestBed.configureTestingModule({
        imports: [FormsModule, RouterTestingModule, HttpClientTestingModule,
          HttpModule],
        declarations: [DashboardComponent, RouterOutletStubComponent, RouterLinkDirectiveStub, NavBarComponentStubComponent],
        providers: [
          { provide: CarService, useValue: carServiceSpy },
          { provide: CustomerService, useValue: customerServiceSpy },
          { provide: RentalService, useValue: rentalServiceSpy }
        ]
      })
        .compileComponents().then(() => {
          fixture = TestBed.createComponent(DashboardComponent);
          dashboardComponent = fixture.componentInstance;
        });

        carServiceSpy.getCars.and.returnValue(asyncData(cars));
        rentalServiceSpy.getRentals.and.returnValue(asyncData(rentals));
        customerServiceSpy.getCustomers.and.returnValue(asyncData(customers));
        customerServiceSpy.initCurrentUser.and.returnValue(asyncData(customers[0]));
    }));

    beforeEach(() => {
      fixture = TestBed.createComponent(DashboardComponent);
      dashboardComponent = fixture.componentInstance;
      fixture.detectChanges();
    });

    beforeEach(() => {
      fixture.detectChanges(); 
      linkDes = fixture.debugElement
        .queryAll(By.directive(RouterLinkDirectiveStub));
      routerLinks = linkDes.map(de => de.injector.get(RouterLinkDirectiveStub));
    });

    it('should create', () => {
      expect(dashboardComponent).toBeTruthy();
    });

    it('can instantiate the component', () => {
      expect(dashboardComponent).not.toBeNull();
    });
 

    it('can get RouterLinks from template', () => {
      expect(routerLinks.length).toBe(0, 'should have 0 routerLinks');
    });

    it('should NOT have cars before ngOnInit', () => {
      expect(dashboardComponent.cars.length).toBe(0,
        'should not have cars before ngOnInit');
    });

    it('should NOT have cars immediately after ngOnInit', () => {
      fixture.detectChanges(); 
      expect(dashboardComponent.cars.length).toBe(0,
        'should not have cars until service promise resolves');
    });

    it('should NOT have customers before ngOnInit', () => {
    expect(dashboardComponent.customers.length).toBe(0,
      'should not have customers before ngOnInit');
  });

  it('should NOT have customers immediately after ngOnInit', () => {
    fixture.detectChanges(); // runs initial lifecycle hooks

    expect(dashboardComponent.customers.length).toBe(0,
      'should not have customers until service promise resolves');
  });

    function asyncData<T> (data: T) {
      return defer(()=> Promise.resolve(data))
    }
});