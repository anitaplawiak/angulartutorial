import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
 
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { catchError, map, tap } from 'rxjs/operators';
 
import { Customer } from './customer';
 
const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable()
export class CustomerService {

  public customersUrl = 'api/customers';  // URL to web api

  constructor(private http: HttpClient) { }

  /** GET customers from the server */
  getCustomers (): Observable<Customer[]> {
    return this.http.get<Customer[]>(this.customersUrl)
      .pipe(
        catchError(this.handleError('getCustomers', []))
      );
  }

  /** GET customer by id. Will 404 if id not found */
  getCustomer(id: number): Observable<Customer> {
    const url = `${this.customersUrl}/${id}`;
    return this.http.get<Customer>(url)
      .pipe(
        catchError(this.handleError<Customer>(`getCustomer id=${id}`))
      );
  }

  /* GET customers whose lastname contains search term */
  searchCustomers(term: string): Observable<Customer[]> {
    if (!term.trim()) {
      // if not search term, return empty customer array.
      return of([]);
    }
    return this.http.get<Customer[]>(`api/customers/?lastname=${term}`)
      .pipe(
        catchError(this.handleError<Customer[]>('searchCustomers', []))
      );
  }

  /** POST: add a new customer to the server */
  addCustomer (customer : Customer): Observable<Customer> {
    return this.http.post<Customer>(this.customersUrl, customer, httpOptions)
      .pipe(
        catchError(this.handleError<Customer>('addCustomer'))
      );
  }

  /** DELETE: delete the customer from the server */
  deleteCustomer (customer: Customer | number): Observable<Customer> {
    const id = typeof customer === 'number' ? customer : customer.id;
    const url = `${this.customersUrl}/${id}`;

    return this.http.delete<Customer>(url, httpOptions)
      .pipe(
        catchError(this.handleError<Customer>('deleteCustomer'))
      );
  }

  /** PUT: update the customer on the server */
  updateCustomer (customer: Customer): Observable<any> {
    return this.http.put(this.customersUrl, customer, httpOptions)
      .pipe(
        catchError(this.handleError<any>('updateCustomer'))
      );
  }

  /**
    * Handle Http operation that failed.
    * Let the app continue.
    * @param operation - lastname of the operation that failed
    * @param result - optional value to return as the observable result
    */
  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}
