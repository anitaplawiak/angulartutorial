import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { Customer } from '../customer';
import { CustomerService } from '../customer.service';

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.css']
})
export class LoginFormComponent implements OnInit {

  customers: Customer[] = [];
  loginError: boolean;

  constructor(private router: Router, private customerService: CustomerService) { }

  ngOnInit() {
    this.getCustomers();
    this.loginError = false;
  }

  getCustomers(): void {
    this.customerService.getCustomers()
      .subscribe(customers => this.customers = customers);
  }

  loginUser(e) {
    e.preventDefault();
    
    var username = e.target.elements[0].value;
    var password = e.target.elements[1].value;

    var loggedUser;

    this.customers.forEach(
      user => {
        if(username == user.email && password == user.password) {
          loggedUser = user;
          localStorage.setItem('currentUser', JSON.stringify(user));
          this.router.navigate(['/dashboard']);
        }
      }
    )

    if(!loggedUser) {
      this.loginError = true;
    }

  }
}
