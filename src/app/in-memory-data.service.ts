import { InMemoryDbService } from 'angular-in-memory-web-api';

export class InMemoryDataService implements InMemoryDbService {
  createDb() {
    let customers = [
      { id: 1, firstname: 'Anita', lastname: 'Pławiak', email: 'a.pla@example.com', password: 'apla', role: 'admin' },
      { id: 2, firstname: 'Wojciech', lastname: 'Czerwiński', email: 'w.cze@example.com', password: 'wcze', role: 'user' },
      { id: 3, firstname: 'Inez', lastname: 'Wilas', email: 'i.wil@example.com', password: 'iwil', role: 'user' },
      { id: 4, firstname: 'Paulina', lastname: 'Laska', email: 'p.las@example.com', password: 'plas', role: 'user' },
      { id: 5, firstname: 'Paulina', lastname: 'Bernard', email: 'p.ber@example.com', password: 'pber', role: 'user' },
      { id: 6, firstname: 'Nadia', lastname: 'Spławik', email: 'n.spl@example.com', password: 'nspl', role: 'user' },
      { id: 7, firstname: 'Damian', lastname: 'Drzewo', email: 'd.drz@example.com', password: 'ddrz', role: 'user' },
      { id: 8, firstname: 'Sylwia', lastname: 'Ostańska', email: 's.ost@example.com', password: 'sost', role: 'user' },
      { id: 9, firstname: 'Dorian', lastname: 'Turkiewicz', email: 'd.tur@example.com', password: 'dtur', role: 'user' },
      { id: 10, firstname: 'Krzysztof', lastname: 'Kurtynka', email: 'k.kur@example.com', password: 'kkur', role: 'user' }
    ];

    let cars = [
      { id: 1, mark: 'Opel', model: 'Astra', colour: 'black', description:'Good car for crazy people', 
        productionDate: '2000-01-01', retirementDate: '2020-12-31', mileage: 250000, servicePeriodStartDate: '2018-02-10', servicePeriodFinishDate: '2018-02-20' },
      { id: 2, mark: 'Opel', model: 'Vectra', colour: 'green', description:'Fast like a rocket', 
      productionDate: '1995-03-01', retirementDate: '2030-09-25', mileage: 100000, servicePeriodStartDate: '2018-04-15', servicePeriodFinishDate: '2018-04-18'  },
      { id: 3, mark: 'Audi', model: 'A4', colour: 'orange', description:'You got to have it', 
      productionDate: '2005-09-25', retirementDate: '2025-09-25', mileage: 300000, servicePeriodStartDate: '2019-05-15', servicePeriodFinishDate: '2019-05-31'   },
      { id: 4, mark: 'Audi', model: 'A6', colour: 'yellow', description:'The car of your dreams', 
      productionDate: '2001-08-13', retirementDate: '2021-01-01', mileage: 150000, servicePeriodStartDate: '2018-02-10', servicePeriodFinishDate: '2018-02-20'  },
      { id: 5, mark: 'Audi', model: 'A5', colour: 'pink', description:'Sexy car for sexy women', 
      productionDate: '2010-01-13', retirementDate: '2020-01-01', mileage: 100000, servicePeriodStartDate: '2018-04-27', servicePeriodFinishDate: '2018-05-01'  },
      { id: 6, mark: 'Audi', model: 'A7', colour: 'red', description:'Red cars are the fastest', 
      productionDate: '2015-05-26', retirementDate: '2030-01-01', mileage: 50000, servicePeriodStartDate: '2019-06-01', servicePeriodFinishDate: '2019-06-30'  },
      { id: 7, mark: 'Fiat', model: '125p', colour: 'white', description:'Oldschool', 
      productionDate: '1996-01-01', retirementDate: '2100-01-01', mileage: 500000, servicePeriodStartDate: '2018-04-01', servicePeriodFinishDate: '2018-04-30'  },
      { id: 8, mark: 'Mercedes-Benz', model: 'Klasa A', colour: 'silver', description:'Be careful with the star', 
      productionDate: '2005-01-01', retirementDate: '2019-12-31', mileage: 146000, servicePeriodStartDate: '2018-05-10', servicePeriodFinishDate: '2018-05-11'  },
      { id: 9, mark: 'Mercedes-Benz', model: 'Klasa S', colour: 'gold', description:'Car is your best friend', 
      productionDate: '2017-12-31', retirementDate: '2030-12-31', mileage: 10000, servicePeriodStartDate: '2018-06-18', servicePeriodFinishDate: '2018-06-25' },
      { id: 10, mark: 'Subaru', model: 'Impreza', colour: 'green', description:'Car is your business card', 
      productionDate: '2018-01-01', retirementDate: '2040-12-31', mileage: 0, servicePeriodStartDate: '2018-12-20', servicePeriodFinishDate: '2018-12-22'  }
    ];

    let rentals = [
      { id: 1, startDate: new Date('2018-03-15'), finishDate: new Date('2018-03-22'), carId: 1, customerId: 1, accepted: true },
      { id: 2, startDate: new Date('2018-01-05'), finishDate: new Date('2018-01-09'), carId: 2, customerId: 3, accepted: true },
      { id: 3, startDate: new Date('2018-01-13'), finishDate: new Date('2018-01-21'), carId: 3, customerId: 3, accepted: true  },
      { id: 4, startDate: new Date('2018-01-19'), finishDate: new Date('2018-01-25'), carId: 4, customerId: 8, accepted: true  },
      { id: 5, startDate: new Date('2018-04-18'), finishDate: new Date('2018-04-19'), carId: 5, customerId: 4, accepted: true  },
      { id: 6, startDate: new Date('2018-02-14'), finishDate: new Date('2018-03-08'), carId: 6, customerId: 7, accepted: true  },
      { id: 7, startDate: new Date('2018-02-28'), finishDate: new Date('2018-03-08'), carId: 7, customerId: 5, accepted: true  },
      { id: 8, startDate: new Date('2018-03-26'), finishDate: new Date('2018-03-31'), carId: 8, customerId: 9, accepted: true },
      { id: 9, startDate: new Date('2018-04-01'), finishDate: new Date('2018-04-10'), carId: 8, customerId: 1, accepted: true },
      { id: 10, startDate: new Date('2018-04-10'), finishDate: new Date('2018-04-15'), carId: 8, customerId: 2, accepted: true }
    ];

    return {cars, customers, rentals};
  }
}