export class Rental {
    id: number;
    startDate: Date;
    finishDate: Date;
    carId: number;
    customerId: number;
    accepted: boolean;
  }