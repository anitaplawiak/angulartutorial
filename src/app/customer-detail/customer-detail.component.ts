import { Component, OnInit, Input } from '@angular/core';
import { DataTableResource } from 'angular5-data-table';

import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

import { Customer } from '../customer';
import { CustomerService }  from '../customer.service';

import { Rental } from '../rental';
import { RentalService } from '../rental.service';

@Component({
  selector: 'app-customer-detail',
  templateUrl: './customer-detail.component.html',
  styleUrls: ['./customer-detail.component.css']
})
export class CustomerDetailComponent implements OnInit {

  @Input() customer: Customer;
  rentals: Rental[];

  tableResource: DataTableResource<Rental>;
  items: Rental[] = [];
  itemCount: number;

  constructor(
    private route: ActivatedRoute,
    private customerService: CustomerService,
    private rentalService: RentalService,
    private location: Location
  ) {}

  ngOnInit(): void {
    this.getCustomer();
    this.getRentals();
  }

  getCustomer(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    this.customerService.getCustomer(id)
      .subscribe(customer => this.customer = customer);
  }

  getRentals(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    this.rentalService.getRentalsForCustomer(id)
      .subscribe(rentals => {
        this.rentals = rentals.filter(r => r.customerId == id);
        this.initializeTable(rentals.filter(r => r.customerId == id));
      });
  }

  private initializeTable(rentals: Rental[]) {
    this.tableResource = new DataTableResource(rentals);
    this.tableResource.query({ offset: 0 })
      .then(items => this.items = items);
    this.tableResource.count()
      .then(count => this.itemCount = count);
  }

  reloadItems(params) {
    if(!this.tableResource)
      return;

    this.tableResource.query(params)
        .then(items => this.items = items);
  }

  goBack(): void {
    this.location.back();
  }

  save(): void {
    this.customerService.updateCustomer(this.customer)
      .subscribe(() => this.goBack());
  }

}
