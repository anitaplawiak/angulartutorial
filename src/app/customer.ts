export class Customer {
    id: number;
    firstname: string;
    lastname: string;
    email: string;
    password: string;
    role: string;
  }