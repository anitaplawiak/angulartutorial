import { Component, OnInit, TemplateRef } from '@angular/core';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { DataTableResource } from 'angular5-data-table';
import { DatePipe } from '@angular/common';

import { Car } from '../car';
import { CarService } from '../car.service';

import { Rental } from '../rental';
import { RentalService } from '../rental.service';

import { Customer } from '../customer';

@Component({
  selector: 'app-car-reservation',
  templateUrl: './car-reservation.component.html',
  styleUrls: ['./car-reservation.component.css']
})
export class CarReservationComponent implements OnInit {
  
  cars: Car[];
  rentals: Rental[];
  currentUser: Customer;

  // Variables for DateRangePicker
  bsValue = new Date();
  bsRangeValue: Date[];
  maxDate = new Date();

  // New rental date period variables
  dateFrom: Date;
  dateTo: Date;

  // Variables for Modal window and angular5-data-table
  modalRef: BsModalRef;
  tableResource: DataTableResource<Car>;
  items: Car[] = [];
  itemCount: number;

  constructor(private carService: CarService, private rentalService: RentalService, 
    private modalService: BsModalService, private datepipe: DatePipe) {

    this.maxDate.setDate(this.maxDate.getDate() + 1);
    this.bsRangeValue = [this.bsValue, this.maxDate];
    this.dateFrom = this.bsValue;
    this.dateTo = this.maxDate;
  }

  ngOnInit() {
    this.initCurrentUser();
    this.reloadTable();
  }

  initCurrentUser(): void {
    var user = localStorage.getItem('currentUser');
    this.currentUser = JSON.parse(user) as Customer; 
  }

  getRentals(): void {
    this.rentalService.getRentals()
      .subscribe(rentals => 
        this.rentals = this.filterRentals(rentals)
      );
  }

  filterRentals(rentals: Rental[]): Rental[] {
    var filteredRentals = new Array<Rental>();

    rentals.forEach(r => {
      let dateFrom = this.datepipe.transform(this.dateFrom, 'yyyy-MM-dd');
      let dateTo = this.datepipe.transform(this.dateTo, 'yyyy-MM-dd');
      let startDate = this.datepipe.transform(r.startDate, 'yyyy-MM-dd');
      let finishDate = this.datepipe.transform(r.finishDate, 'yyyy-MM-dd');

      if(
          (startDate <= dateFrom && finishDate >= dateTo)
          || (startDate >= dateFrom && startDate <= dateTo)
          || (finishDate >= dateFrom && finishDate <= dateTo)
      ) {
        filteredRentals.push(r);
      }
    });

    return filteredRentals;
  }

  getCars(): void {
    this.carService.getCars()
      .subscribe(cars => {
        this.cars = this.filterCars(cars);
        this.initializeTable(this.cars);
      });
  }

  filterCars(cars: Car[]): Car[] {
    cars = this.filterByRetirementAndServiceTime(cars);
    cars = this.filterByRentals(cars);

    return cars;
  }

  filterByRetirementAndServiceTime(cars: Car[]): Car[] {
    var filteredCars = new Array<Car>();
    
    cars.forEach(c => {
        let dateFrom = this.datepipe.transform(this.dateFrom, 'yyyy-MM-dd');
        let dateTo = this.datepipe.transform(this.dateTo, 'yyyy-MM-dd');
        let retireDate = this.datepipe.transform(c.retirementDate, 'yyyy-MM-dd');
        let serviceStart = this.datepipe.transform(c.servicePeriodStartDate, 'yyyy-MM-dd');
        let serviceFinish = this.datepipe.transform(c.servicePeriodFinishDate, 'yyyy-MM-dd');

        if (dateTo < retireDate 
          && !((serviceStart <= dateFrom && serviceStart >= dateTo)
              || (serviceStart >= dateFrom && serviceStart <= dateTo)
              || (serviceFinish >= dateFrom && serviceFinish <= dateTo))) {
          filteredCars.push(c);
        }
      }
    );

    return filteredCars;
  }

  filterByRentals(cars: Car[]): Car[] {
    var filteredCars = new Array<Car>();
    var isAlreadyReserved;
    
    cars.forEach(c => {
        isAlreadyReserved = false;
        this.rentals.forEach(r => {
          if(c.id == r.carId) {
            isAlreadyReserved = true;
          }
        })

        if(!isAlreadyReserved) { 
          filteredCars.push(c);
        }
      }
    );

    return filteredCars;
  }

  private initializeTable(cars: Car[]) {
    this.tableResource = new DataTableResource(cars);
    this.tableResource.query({ offset: 0 })
      .then(items => this.items = items);
    this.tableResource.count()
      .then(count => this.itemCount = count);
  }

  reloadItems(params) {
    if(!this.tableResource)
      return;

    this.tableResource.query(params)
        .then(items => this.items = items);
  }

  reloadTable() {
    if (this.dateFrom != undefined 
      && this.dateTo != undefined
    ) {
      this.getRentals();
      this.getCars();
    }
  }

  onTimerangeChange(value: Date[]): void {
    if (value != null && value != undefined)
    {
      this.dateFrom = new Date(this.datepipe.transform(value[0], 'yyyy-MM-dd'));
      this.dateTo = new Date(this.datepipe.transform(value[1], 'yyyy-MM-dd'));
      this.reloadTable();
    }  
  }

  openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template, {class: 'modal-sm'});
  }

  modalSubmitted(carId: number) {
    this.modalRef.hide();
    
    this.addRental(carId);
    this.reloadTable();
  }

  addRental(carId: number){
    var startDate = this.dateFrom;
    var finishDate = this.dateTo;
    var carId = carId;
    var customerId = this.currentUser.id;
    var accepted = false;

    this.rentalService.addRental({ startDate, finishDate, carId, customerId, accepted } as Rental)
      .subscribe(rental => {
        this.rentals.push(rental);
      });
  }
}
