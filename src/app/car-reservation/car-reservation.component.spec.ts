import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CarReservationComponent } from './car-reservation.component';

describe('CarReservationComponent', () => {
  let carReservationComponent: CarReservationComponent;
  let fixture: ComponentFixture<CarReservationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CarReservationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CarReservationComponent);
    carReservationComponent = fixture.componentInstance;
    fixture.detectChanges();
  });
});
