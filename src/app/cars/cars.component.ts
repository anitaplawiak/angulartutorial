import { Component, OnInit, TemplateRef } from '@angular/core';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { DataTableResource } from 'angular5-data-table';
import { Car } from '../car';
import { CarService } from '../car.service';

@Component({
  selector: 'app-cars',
  templateUrl: './cars.component.html',
  styleUrls: ['./cars.component.css']
})
export class CarsComponent implements OnInit {

  cars: Car[];
  modalRef: BsModalRef;

  tableResource: DataTableResource<Car>;
  items: Car[] = [];
  itemCount: number;

  constructor(private carService: CarService, private modalService: BsModalService) { }

  ngOnInit() {
    this.getCars();
  }

  getCars(): void {
    this.carService.getCars()
      .subscribe(cars => { 
        this.cars = cars;
        this.initializeTable(cars);
      });
  }

  private initializeTable(cars: Car[]) {
    this.tableResource = new DataTableResource(cars);
    this.tableResource.query({ offset: 0 })
      .then(items => this.items = items);
    this.tableResource.count()
      .then(count => this.itemCount = count);
  }

  reloadItems(params) {
    if(!this.tableResource)
      return;

    this.tableResource.query(params)
        .then(items => this.items = items);
  }

  add(mark: string): void {
    mark = mark.trim();
    if (!mark) { return; }
    this.carService.addCar({ mark } as Car)
      .subscribe(car => {
        this.cars.push(car);
      });
  }

  addCar(e){
    this.modalRef.hide();

    e.preventDefault();

    var mark = e.target.elements[0].value;
    var model = e.target.elements[1].value;
    var colour = e.target.elements[2].value;
    var productionDate = e.target.elements[3].value;
    var retirementDate = e.target.elements[4].value;
    var mileage = e.target.elements[5].value;
    var servicePeriodStartDate = e.target.elements[6].value;
    var servicePeriodFinishDate = e.target.elements[7].value;

    this.carService.addCar({ mark, model, colour, productionDate, retirementDate, mileage, servicePeriodStartDate, servicePeriodFinishDate  } as Car)
      .subscribe(car => {
        this.cars.push(car);
      });

    this.getCars();
  }

  delete(car: Car): void {
    this.cars = this.cars.filter(h => h !== car);
    this.carService.deleteCar(car).subscribe();

    this.getCars();
  }

  filter(query: string) {
    let filteredCars = (query) ?
      this.cars.filter(c => 
        query.includes(String(c.id))
        || c.mark.toLowerCase().includes(query.toLowerCase())
        || c.model.toLowerCase().includes(query.toLowerCase())
        || c.colour.toLowerCase().includes(query.toLowerCase())
      ) :
      this.cars;
    
    this.initializeTable(filteredCars);
  }
  openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template, {class: 'modal-sm'});
  }

}
