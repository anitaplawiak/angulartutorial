import { Component, OnInit, TemplateRef } from '@angular/core';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { DataTableResource } from 'angular5-data-table';

import { Customer } from '../customer';
import { CustomerService } from '../customer.service';

@Component({
  selector: 'app-customers',
  templateUrl: './customers.component.html',
  styleUrls: ['./customers.component.css']
})
export class CustomersComponent implements OnInit {

  customers: Customer[];
  // komunikat: boolean;
  modalRef: BsModalRef;

  tableResource: DataTableResource<Customer>;
  items: Customer[] = [];
  itemCount: number;

  constructor(private customerService: CustomerService, private modalService: BsModalService) {

  }

  ngOnInit() {
    this.getCustomers();
  }

  getCustomers(): void {
    this.customerService.getCustomers()
      .subscribe(customers => { 
        this.customers = customers;
        this.initializeTable(customers);
      });
  }

  private initializeTable(customers: Customer[]) {
    this.tableResource = new DataTableResource(customers);
    this.tableResource.query({ offset: 0 })
      .then(items => this.items = items);
    this.tableResource.count()
      .then(count => this.itemCount = count);
  }

  reloadItems(params) {
    if(!this.tableResource)
      return;

    this.tableResource.query(params)
        .then(items => this.items = items);
  }

  addCustomer(e){
    this.modalRef.hide();

    e.preventDefault();

    var firstname = e.target.elements[0].value;
    var lastname = e.target.elements[1].value;
    var email = e.target.elements[2].value;

    this.customerService.addCustomer({ firstname, lastname, email } as Customer)
      .subscribe(customer => {
        this.customers.push(customer);
      });

    this.getCustomers();
  }

  delete(customer: Customer): void {
    this.customers = this.customers.filter(c => c !== customer);
    this.customerService.deleteCustomer(customer).subscribe();
    
    this.getCustomers();
  }

  filter(query: string) {
    let filteredCustomers = (query) ?
      this.customers.filter(c => 
        query.includes(String(c.id))
        || c.firstname.toLowerCase().includes(query.toLowerCase())
        || c.lastname.toLowerCase().includes(query.toLowerCase())
        || c.email.toLowerCase().includes(query.toLowerCase())
      ) :
      this.customers;
    
    this.initializeTable(filteredCustomers);
  }

  openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template, {class: 'modal-sm'});
  }

}
