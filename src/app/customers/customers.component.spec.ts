import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomersComponent } from './customers.component';
import { NavBarComponent } from '../nav-bar/nav-bar.component';

describe('CustomersComponent', () => {
  let customerComponent: CustomersComponent;
  let fixture: ComponentFixture<CustomersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustomersComponent, NavBarComponent   ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomersComponent);
    customerComponent = fixture.componentInstance;
    fixture.detectChanges();
  });
});
