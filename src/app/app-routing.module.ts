import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { DashboardComponent }   from './dashboard/dashboard.component';
import { CustomersComponent }      from './customers/customers.component';
import { CustomerDetailComponent }  from './customer-detail/customer-detail.component';
import { CarsComponent } from './cars/cars.component';
import { CarDetailComponent }  from './car-detail/car-detail.component';
import { RentalsComponent } from './rentals/rentals.component';
import { RentalDetailComponent }  from './rental-detail/rental-detail.component';
import { LoginFormComponent } from './login-form/login-form.component';
import { CarReservationComponent } from './car-reservation/car-reservation.component';

const routes: Routes = [
  { path: '', redirectTo: '/login-form', pathMatch: 'full' },
  { path: 'login-form', component: LoginFormComponent },
  { path: 'dashboard', component: DashboardComponent },
  { path: 'customers', component: CustomersComponent },
  { path: 'customer/:id', component: CustomerDetailComponent },
  { path: 'cars', component: CarsComponent },
  { path: 'car/:id', component: CarDetailComponent },
  { path: 'rentals', component: RentalsComponent },
  { path: 'rental/:id', component: RentalDetailComponent },
  { path: 'car-reservation', component: CarReservationComponent },
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}
