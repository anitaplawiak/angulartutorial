import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
 
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { catchError, map, tap } from 'rxjs/operators';
 
import { Rental } from './rental';
 
const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable()
export class RentalService {

  private rentalsUrl = 'api/rentals';  // URL to web api

  constructor(
    private http: HttpClient) { }

  /** GET rentals from the server */
  getRentals(): Observable<Rental[]> {
    return this.http.get<Rental[]>(this.rentalsUrl)
      .pipe(
        catchError(this.handleError('getRentals', []))
      );
  }

  getRentalsForCustomer(id: number): Observable<Rental[]> {
    return this.http.get<Rental[]>(this.rentalsUrl)
      .pipe(
        catchError(this.handleError('getRentalsForCustomer', []))
      );
  }

  /** GET rental by id. Will 404 if id not found */
  getRental(id: number): Observable<Rental> {
    const url = `${this.rentalsUrl}/${id}`;
    return this.http.get<Rental>(url)
      .pipe(
        catchError(this.handleError<Rental>(`getRental id=${id}`))
      );
  }

  /* GET rentals whose startDate contains search term */
  searchRentals(term: string): Observable<Rental[]> {
    if (!term.trim()) {
      // if not search term, return empty rental array.
      return of([]);
    }
    return this.http.get<Rental[]>(`api/rentals/?startDate=${term}`)
      .pipe(
        catchError(this.handleError<Rental[]>('searchRentals', []))
      );
  }

  /** POST: add a new rental to the server */
  addRental (rental : Rental): Observable<Rental> {
    return this.http.post<Rental>(this.rentalsUrl, rental, httpOptions)
      .pipe(
        catchError(this.handleError<Rental>('addRental'))
      );
      
  }

  /** DELETE: delete the rental from the server */
  deleteRental (rental: Rental | number): Observable<Rental> {
    const id = typeof rental === 'number' ? rental : rental.id;
    const url = `${this.rentalsUrl}/${id}`;

    return this.http.delete<Rental>(url, httpOptions)
      .pipe(
        catchError(this.handleError<Rental>('deleteRental'))
      );
  }

  /** PUT: update the rental on the server */
  updateRental (rental: Rental): Observable<any> {
    return this.http.put(this.rentalsUrl, rental, httpOptions)
      .pipe(
        catchError(this.handleError<any>('updateRental'))
      );
  }

  /**
    * Handle Http operation that failed.
    * Let the app continue.
    * @param operation - startDate of the operation that failed
    * @param result - optional value to return as the observable result
    */
  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}
