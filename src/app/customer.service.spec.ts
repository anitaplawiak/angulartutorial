import { TestBed, inject } from '@angular/core/testing';
import { HttpClient, HttpResponse, HttpErrorResponse } from '@angular/common/http';


import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { CustomerService } from './customer.service';
import { Customer } from './customer';
import { asyncData, asyncError } from '../testing/async-observable-helpers';



describe('CustomerService (with spies)', () => {
  let httpClientSpy: { get: jasmine.Spy, delete: jasmine.Spy, post: jasmine.Spy };
  let customerService: CustomerService;
 
  beforeEach(() => {
    // TODO: spy on other methods too
    httpClientSpy = jasmine.createSpyObj('HttpClient', ['get', 'delete', 'post']);
    customerService = new CustomerService(<any> httpClientSpy);
 
    TestBed.configureTestingModule({
      providers: [CustomerService, {provide: HttpClient, useValue: httpClientSpy }]
    });
  });



  it('should be created', inject([CustomerService], (customerService:CustomerService) => {
    expect(customerService).toBeTruthy();
  }));



  it('should return expected customers (HttpClient called once)', () => {
    const expectedCustomers: Customer[] =
      [{ id: 1, firstname: 'Ala', lastname: "Krakowiak", email: "a.kra@example.com", password: 'akra', role: 'user' },
      { id: 2, firstname: 'Krzysztof', lastname: "Szczypta", email: "k.szcz@example.com", password: 'ksz', role: 'user' }];

    httpClientSpy.get.and.returnValue(asyncData(expectedCustomers));

    customerService.getCustomers().subscribe(
      customers => expect(customers).toEqual(expectedCustomers, 'expected customers'),
      fail
    );
    expect(httpClientSpy.get.calls.count()).toBe(1, 'one call');
  });



  it('should return deleted customer (HttpClient called once)', () => {
    const deletedCustomer: Customer[] =
      [{ id: 1, firstname: 'Ala', lastname: "Krakowiak", email: "a.kra@example.com", password: 'akra', role: 'user' }];

    httpClientSpy.delete.and.returnValue(asyncData(deletedCustomer[0]));

    customerService.deleteCustomer(deletedCustomer[0]).subscribe(
      customer => {
        // console.log(customer);
        // console.log(deletedCustomer[0]);
        expect(customer).toEqual(deletedCustomer[0], 'deleted customers');
      },
      fail
    );
    expect(httpClientSpy.delete.calls.count()).toBe(1, 'one call');
  });


  it('should return added customer (HttpClient called once)', () => {
    const addedCustomer: Customer[] =
      [{ id: 2, firstname: 'Zosia', lastname: "Samosia", email: "z.sam@example.com", password: 'zsam', role: 'user' },];
  
    httpClientSpy.post.and.returnValue(asyncData(addedCustomer[0]));
  
    customerService.addCustomer(addedCustomer[0]).subscribe(
      customer => expect(customer).toEqual(addedCustomer[0], 'added customers'),
      fail
    );
    expect(httpClientSpy.post.calls.count()).toBe(1, 'one call');
  });



  it('should return searched customers (HttpClient called once)', () => {
    const expectedCustomers: Customer[] =
      [{ id: 3, firstname: 'Jacek', lastname: "Jackowiak", email: "j.jac@example.com", password: 'jaco', role: 'user' },];

    httpClientSpy.get.and.returnValue(asyncData(expectedCustomers));

    customerService.searchCustomers('Jacek').subscribe(
      customers => expect(customers).toEqual(expectedCustomers, 'expected customers'),
      fail
    );
    expect(httpClientSpy.get.calls.count()).toBe(1, 'one call');
  });
});






describe('CustomerService (with mocks)', () => {
  let httpClient: HttpClient;
  let httpTestingController: HttpTestingController;
  let customerService: CustomerService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      // Import the HttpClient mocking services
      imports: [ HttpClientTestingModule ],
      // Provide the service-under-test
      providers: [ CustomerService ]
    });

    // Inject the http, test controller, and service-under-test
    // as they will be referenced by each test.
    httpClient = TestBed.get(HttpClient);
    httpTestingController = TestBed.get(HttpTestingController);
    customerService = TestBed.get(CustomerService);
  });

  afterEach(() => {
    // After every test, assert that there are no more pending requests.
    httpTestingController.verify();
  });

  describe('#getCustomers', () => {
    let expectedCustomers: Customer[];

    beforeEach(() => {
      customerService = TestBed.get(CustomerService);
      expectedCustomers = [
        { id: 1, firstname: 'Ala', lastname: "Krakowiak", email: "a.kra@example.com", password: 'akra', role: 'user' },
      { id: 2, firstname: 'Krzysztof', lastname: "Szczypta", email: "k.szcz@example.com", password: 'ksz', role: 'user' },
    ] as Customer[];
    });

    
    it('should return expected customers (called once)', () => {
      customerService.getCustomers().subscribe(
        customers => expect(customers).toEqual(expectedCustomers, 'should return expected customers'),
        fail
      );

      // CustomerService should have made one request to GET customers from expected URL
      const req = httpTestingController.expectOne(customerService.customersUrl);
      expect(req.request.method).toEqual('GET');

      // Respond with the mock customers
      req.flush(expectedCustomers);
    });

    it('should be OK returning no customers', () => {
      customerService.getCustomers().subscribe(
        customers => expect(customers.length).toEqual(0, 'should have empty customers array'),
        fail
      );

      const req = httpTestingController.expectOne(customerService.customersUrl);
      req.flush([]); // Respond with no customers
    });

    it('should return expected customers (called multiple times)', () => {
      customerService.getCustomers().subscribe();
      customerService.getCustomers().subscribe();
      customerService.getCustomers().subscribe(
        customers => expect(customers).toEqual(expectedCustomers, 'should return expected customers'),
        fail
      );

      const requests = httpTestingController.match(customerService.customersUrl);
      expect(requests.length).toEqual(3, 'calls to getCustomers()');

      // Respond to each request with different mock customer results
      requests[0].flush([]);
      requests[1].flush([{id: 1, name: 'bob'}]);
      requests[2].flush(expectedCustomers);
    });
  });

  describe('#updateCustomer', () => {
    // Expecting the query form of URL so should not 404 when id not found
    const makeUrl = (id: number) => `${customerService.customersUrl}/?id=${id}`;

    it('should update a customer and return it', () => {

      const updateCustomer: Customer =  { id: 1, firstname: 'Ala', lastname: "Krakowiak", email: "a.kra@example.com", password: 'akra', role: 'user' };

      customerService.updateCustomer(updateCustomer).subscribe(
        data => expect(data).toEqual(updateCustomer, 'should return the customer'),
        fail
      );

      // CustomerService should have made one request to PUT customer
      const req = httpTestingController.expectOne(customerService.customersUrl);
      expect(req.request.method).toEqual('PUT');
      expect(req.request.body).toEqual(updateCustomer);

      // Expect server to return the customer after PUT
      const expectedResponse = new HttpResponse(
        { status: 200, statusText: 'OK', body: updateCustomer });
      req.event(expectedResponse);
    });
  });
});