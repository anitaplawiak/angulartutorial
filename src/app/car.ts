export class Car {
    id: number;
    mark: string;
    model: string;
    colour: string;
    productionDate: Date;
    retirementDate: Date;
    mileage: number;
    servicePeriodStartDate: Date;
    servicePeriodFinishDate: Date;
  }