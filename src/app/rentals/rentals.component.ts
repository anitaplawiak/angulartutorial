import { Component, OnInit, TemplateRef } from '@angular/core';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { DataTableResource } from 'angular5-data-table';

import { Rental } from '../rental';
import { RentalService } from '../rental.service';

@Component({
  selector: 'app-rentals',
  templateUrl: './rentals.component.html',
  styleUrls: ['./rentals.component.css']
})
export class RentalsComponent implements OnInit {

  rentals: Rental[];
  modalRef: BsModalRef;

  tableResource: DataTableResource<Rental>;
  items: Rental[] = [];
  itemCount: number;

  constructor(private rentalService: RentalService, private modalService: BsModalService) { }

  ngOnInit() {
    this.getRentals();
  }

  getRentals(): void {
    this.rentalService.getRentals()
    .subscribe(rentals => { 
      this.rentals = rentals;
      this.initializeTable(rentals);
    });
  }

  private initializeTable(rentals: Rental[]) {
    this.tableResource = new DataTableResource(rentals);
    this.tableResource.query({ offset: 0 })
      .then(items => this.items = items);
    this.tableResource.count()
      .then(count => this.itemCount = count);
  }

  reloadItems(params) {
    if(!this.tableResource)
      return;

    this.tableResource.query(params)
        .then(items => this.items = items);
  }

  // add(startDate: string): void {
  //   startDate = startDate.trim();
  //   if (!startDate) { return; }
  //   this.rentalService.addRental({ startDate } as Rental)
  //     .subscribe(rental => {
  //       this.rentals.push(rental);
  //     });
  // }

  addRental(e){
    this.modalRef.hide();

    e.preventDefault();

    var startDate = e.target.elements[0].value;
    var finishDate = e.target.elements[1].value;
    var carId = e.target.elements[2].value;
    var customerId = e.target.elements[3].value;
    var accepted = e.target.elements[4].value;

    this.rentalService.addRental({ startDate, finishDate, carId, customerId, accepted } as Rental)
      .subscribe(rental => {
        this.rentals.push(rental);
      });

    this.getRentals();
  }

  delete(rental: Rental): void {
    this.rentals = this.rentals.filter(h => h !== rental);
    this.rentalService.deleteRental(rental).subscribe();

    this.getRentals();
  }

  filter(query: string) {
    let filteredRentals = (query) ?
      this.rentals.filter(c => 
        query.includes(String(c.id))
        || query.includes(String(c.startDate))
        || query.includes(String(c.finishDate))
        || query.includes(String(c.carId))
        || query.includes(String(c.customerId))
        || query.includes(String(c.accepted))
      ) :
      this.rentals;
    
    this.initializeTable(filteredRentals);
  }

  openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template, {class: 'modal-sm'});
  }

}
